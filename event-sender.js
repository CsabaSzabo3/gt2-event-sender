/*
 * Example of use:
 *
 * (function() {
        // Instantiate the GT2EventSender class
 *      sender = GT2EventSender(null, 'example game', '1.0', canvas_context);
 *
 *      // Send the game_load message
 *      sender.send(GT2EventSender.LOAD, payload);
 *
 *      // Send the game_start message
 *      sender.send(GT2EventSender.START, payload);
 *
 *      // Send the first game event
 *      sender.send(GT2EventSender.EVENT, payload);
 *
 *      // Reserve a msg_num here.  This can be useful e.g. for keystroke
 *      // events: the message number is reserved when the keypress
 *      // happends…
 *      var second_event_num = sender.reserve_msg_num();
 *
 *      // …then we send another event that actually happened after the
 *      // keypress, but before releasing the key…
 *      sender.send(EventSender.EVENT, payload);
 *
 *      // …and finally we send the keypress event when the key is released,
 *      // with the correct message number.  This way we can include the
 *      // calculated keypress length, too.
 *      var second_event_timestamp = sender.get_msg_num_timestamp(second_event_num);
 *      // Do the calculation
 *      sender.send(EventSender.EVENT, payload, second_event_num);
 *
 *      // At the end, send the game_end event
 *      sender.send(EventSender.END, payload);
 * })
 */

/**
 * This callback type is called `requestCallback` and is displayed as a global symbol.
 *
 * @callback requestCallback
 * @param {boolean} success - if the request was successful
 * @param {Object} response - response object is case of successful response
 * @param {string} error - error message is case of not successful response
 */

const DEFAULT_CONFIG = {
    event_url: 'https://benchmarked.games/api/v1/game-data/',
    retry_count: 3,
    event_send_timeout: 10000,
    send_events: true,
    log_events: false
};

/**
 * GT2 Game Event sending class
 * @constructor
 *
 * Conforms with GT2 Game Message Standard version 1.
 *
 * @param {object} config - Configuration data
 * @param {String} game_name - The name of the game
 * @param {String} game_version - The version of the game
 * @param {Object} game_context - The canvas context of the game, if any
 */
class EventSender {
    constructor(config, game_name, game_version, game_context) {
        this.game_name = game_name;
        this.game_version = game_version;
        this.game_context = game_context;
        this.game_id = null;
        this.gameplay_id = null;
        this.gameplay_session_id = null;
        this.player_id = null;

        var queryParams = {};

        window.location.search.substr(1).split('&').forEach(function (pair) {
            if (pair === '') return;

            var parts = pair.split('=');
            queryParams[parts[0]] = parts[1] &&
                decodeURIComponent(
                    parts[1].replace(/\+/g, ' '));
        });

        if ((config === undefined) || (config === null) || (config === {})) {
            config = DEFAULT_CONFIG;
        }

        this.game_id = queryParams.game_id;
        this.gameplay_id = queryParams.gameplay_id;
        this.player_id = queryParams.player_id;

        if (config.hasOwnProperty('send_events')) {
            this.send_events = !!config.send_events;
        } else {
            this.send_events = true;
        }

        if (config.hasOwnProperty('log_events')) {
            this.log_events = !!config.log_events;
        } else {
            this.log_events = false;
        }

        if (config.hasOwnProperty('event_url')) {
            this.event_url = config.event_url;
        }

        if (config.hasOwnProperty('retry_count')) {
            this.retry_count = Number(config.retry_count);
        } else {
            this.retry_count = 3;
        }

        if (config.hasOwnProperty('event_send_timeout')) {
            this.timeout = Number(config.event_send_timeout);
        } else {
            this.timeout = 10000;
        }

        // Error checking
        if (this.send_events && !this.event_url) {
            throw new Error('Event URL is not configured');
        }

        // Initial values
        this.msg_num = 0;
        this.load_timestamp = null;
        this.load_sent = false;
        this.start_sent = false;
        this.end_sent = false;
        this.msg_num_timestamps = {};
        this.gameplay_session_id = this.generateSessionId();

        this.xhr = new XMLHttpRequest();
    }

    generateSessionId() {
        const sessionIdChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        var ret = 'GS-';

        for (var i = 0; i < 12; i++) {
            var num = Math.floor(Math.random() * (sessionIdChars.length));
            ret += sessionIdChars[num];
        }

        return ret;
    }

    /**
     * Reserve a message number for later use.  This is useful for events that
     * should be sent later (like keypresses tat incorporate keypress length),
     * but on the event timeline, happen earlier than some others.
     */
    reserve_msg_num() {
        var msg_num = ++this.msg_num;

        this.msg_num_timestamps[msg_num] = Date.now();

        return msg_num;
    };

    /**
     * Get the timestamp when `msg_num` was reserved.
     */
    get_msg_num_timestamp(msg_num) {
        return this.msg_num_timestamps[msg_num];
    }

    /**
     * Send an event with the given payload and optional message number.
     *
     * @param {String} type - the event type.  It is recommended to use the
     *                        EventSender.LOAD, EventSender.START,
     *                        EventSender.EVENT, and EventSender.END constants.
     * @param {Object} payload - the event payload.  May be null.  If not null,
     *                           it must be an object.
     * @param {Number} msg_num - an optional message number.  If not set, one
     *                           will be generated automatically.
     * @param {requestCallback} callback - callback after the event sending
     */
    send(type, payload, msg_num, callback) {
        if ((type !== 'game_load') &&
            (type !== 'game_start') &&
            (type !== 'game_event') &&
            (type !== 'game_end') &&
            this.log_events) {
            throw new Error('Invalid event type!');
        }

        // If the 'game_end' event has already been sent, refuse to send
        // anything
        if (this.end_sent) {
            throw new Error('Trying to send an event after end!');
        }
        if (type === 'game_end') {
            this.end_sent = true;
        }

        // If 'game_load' is already sent, don’t send 'game_load' again.
        if ((this.load_sent) && type === 'game_load') {
            throw new Error('Trying to send load for a second time!');
        }

        if (type === 'game_load') {
            this.load_sent = true;

            if ((payload === undefined) || (payload === null)) {
                payload = {};
            }

            var screen = window.screen || {};
            var navigator = window.navigator || {};

            if (this.game_context) {
                var backingStoreRatio =
                    this.game_context.webkitBackingStorePixelRatio ||
                    this.game_context.mozBackingStorePixelRatio ||
                    this.game_context.msBackingStorePixelRatio ||
                    this.game_context.oBackingStorePixelRatio ||
                    this.game_context.backingStorePixelRatio;
                payload['backing-store-ratio'] = backingStoreRatio;
            }

            payload['game-name'] = this.game_name;
            payload['game-version'] = this.game_version;
            payload['user-agent'] = navigator.userAgent;
            payload['platform'] = navigator.platform;
            payload['language'] = navigator.language;
            payload['languages'] = JSON.stringify(navigator.languages);
            payload['screen-avail-height'] = screen.availHeight;
            payload['screen-avail-left'] = screen.availLeft;
            payload['screen-avail-top'] = screen.availTop;
            payload['screen-avail-width'] = screen.availWidth;
            payload['screen-color-depth'] = screen.colorDepth;
            payload['screen-pixel-depth'] = screen.pixelDepth;
            payload['screen-height'] = screen.height;
            payload['screen-width'] = screen.width;
            payload['device-pixel-ratio'] = window.devicePixelRatio;
        }

        if ((this.start_sent) && type === 'game_start') {
            throw new Error('Trying to send start for a second time!');
        }

        if (type === 'game_start') {
            this.start_sent = true;
        }

        // If the message type is 'game_load' or there were no game_load events,
        // set the load timestamp to now
        // TODO: Don't let the game send game_event messages before game_load
        // and game_start are sent, or if game_end is already sent.
        if ((type === 'game_load') || (this.load_timestamp === null)) {
            this.load_timestamp = Date.now();
        }

        // If the message type is 'game_load', the event timestamp is the same
        // as the load timestamp
        var timestamp = (type === 'game_load') ? this.load_timestamp : Date.now();

        // If msg_num is not set, generate a new one.  Otherwise, consider it as
        // a reserved msg_num and take its timestamp from
        // this.msg_num_timestamps (managed by the reserve_msg_num() method), or
        // generate a new one if not present.
        if (msg_num === undefined) {
            msg_num = ++this.msg_num;
        } else {
            timestamp = this.msg_num_timestamps[msg_num] || Date.now();
        }

        var event_data = {
            game_id: this.game_id,
            player_id: this.player_id,
            gameplay_id: this.gameplay_id,
            gameplay_session_id: this.gameplay_session_id,
            msg_num: msg_num,
            timestamp: timestamp,
            msg_type: type,
            relative_time: (timestamp - this.load_timestamp),
            game_payload: payload
        };

        if (this.send_events) {
            var json_data = JSON.stringify(event_data);
            var sender = this;
            var request = new XMLHttpRequest();
            request.open('POST', this.event_url, true);
            request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
            request.timeout = this.timeout;
            request.data = json_data;
            request.tryCount = 0;
            request.retryCount = this.retry_count;

            request.addEventListener('error', function() {
                if (++request.tryCount <= request.retryCount) {
                    request.send(json_data);
                } else {
                    console.error('Could not send event');
                }
            });

            request.addEventListener('readystatechange', function() {
                if (request.readyState == XMLHttpRequest.OPENED) {
                    sender.pendingEvents++;
                } else if (request.readyState == XMLHttpRequest.DONE) {
                    sender.pendingEvents--;

                    var result;

                    try {
                        result = JSON.parse(request.responseText);
                    } catch (error) {
                        console.error('Unexpected response from the API server');
                        if (callback) {
                            callback(false, null, error);
                        }
                        return;
                    }

                    if (result.success === false) {
                        console.error('Event refused');
                        if (callback) {
                            callback(false, null, result.code);
                        }
                    } else {
                        if (callback) {
                            callback(true, result, null);
                        }
                    }
                }
            });

            request.send(json_data);
        }

        if (this.log_events) {
            console.log(event_data);
        }
    }
}

EventSender.LOAD = 'game_load';
EventSender.START = 'game_start';
EventSender.EVENT = 'game_event';
EventSender.END = 'game_end';

export default EventSender;
export {EventSender};
